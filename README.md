#  stackview-in-table-cell

This static version of UI i am trying to bring to life. What follows is screenshot of IB which is schematically what I want to achieve.

![Interface Builder](README/Screenshot 2018-10-22 at 14.10.39.png)

In resulting app the Labels for tags are added programatically. Unfortunately when I run the app, the tags list is not visible :(

![App in simulator](README/Screenshot 2018-10-22 at 14.19.42.png)

This repo serves as a demo for [StackOverflow question](https://stackoverflow.com/questions/52905961/positioning-elements-in-uitableviewcell)
